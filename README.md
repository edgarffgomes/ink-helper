# Tutorial para instalação e execução do projeto:

<ol>
    <li>
        Primeiramente, é necessário que você tenha o <a href="https://git-scm.com/book/pt-br/v2/Come%C3%A7ando-Instalando-o-Git">Git</a>
        e o <a href="https://www.alura.com.br/artigos/instalando-nodejs-no-windows-e-linux?gclid=Cj0KCQjwwJuVBhCAARIsAOPwGATbPPEOwMaJOFbS9YU5tevFhkOI9HP4K6warsCa-bxoijkGYYC_5hUaAteKEALw_wcB">Node JS</a> instalados em sua máquina;
    </li>
    <li>
        Com o Git e NodeJS instalados e configurados, no terminal, dirija-se até o diretório em que deseja clonar (receber) os arquivos do projeto;
    </li>
     <li>
       Execute o comando "git clone https://gitlab.com/edgarffgomes/ink-helper.git" no terminal e aguarde a finalização da clonagem;
    </li>
    <li>
       Após a finalização da clonagem, entre no repositório que você acabou de receber e utilize o comando "npm install" (Este comando instalará os pacotes necessários para executar a aplicação em sua máquina)
    </li>
    <li>
       Após a instalação dos pacotes, execute no terminal o comando "npm start", ele irá preparar a execução do projeto. Caso o projeto não abra automaticamente, insira o link do localhost ,que exibido no terminal após algum tempo, em seu navegador.
    </li>
    <li>
       Pronto, a aplicação já está sendo executada!
    </li>
<ol>
