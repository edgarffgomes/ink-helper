import React from "react";
import "./Introduction.css";
const Introduction = ()=>{

	return(
		<>
			<div className="introContainer">
				<h2>Introdução</h2>
				<p> 
					<span>Olá, tudo bem?</span> 
					Nossa aplicação irá lhe ajudar a calcular
					quantas latas de tintas você precisará para pintar sua sala,
					contudo ela opera com algumas regras:
				</p>
				<ol>
					<li>
						Nenhuma parede pode ter menos de 1 m², nem 
						mais de 50 m²;
					</li>
					<li>
						O total de área das portas e janelas deve ser, no máximo, 50% da área de parede;
					</li>
					<li>
						O total de área das portas e janelas deve ser, no máximo, 50% da área de parede;
					</li>
					<li>
						A altura de paredes com porta deve ser, no mínimo, de 2,20m.
					</li>

					<li>
						As medidas padronizadas das portas são de 0,80m x 1,90m.
					</li>

					<li>
						As medidas padronizadas das janelas são de 2,00m x 1,20m.
					</li>

				</ol>

				<p> 
					Porém, não se preocupe com isso agora. 
					Nosso algoritmo irá lhe informar se alguma dessas regras for quebrada.
					Boa pintura!
				</p>
			</div>
		</>
	)
};
export default Introduction;