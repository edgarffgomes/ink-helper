import React from "react";
import "./ModalResult.css"
const ModalResult = ({setModalActive = () =>{}, count18L, count3L, count2L,  countHalfL })=>{
	
	//função para fechar o modal
	function closeModal(){
		setModalActive(false)
	}

	return(
		<div className="modal"> {/*Início da seção modal*/}
			<div className= "containerModal">
				<header>
						 <span className="result">
						 	Para realizar esta pintura, você precisará de:						 </span>
						 <button onClick={() => closeModal()} className="headerBtn">x</button>
				</header>
				<div className="resultText"> {/*Início seção do texto de resultado*/}
					<p>{count18L} lata(s) de 18 litros</p>
					<p>{count3L} lata(s) de 3,6 litros</p>
					<p>{count2L} lata(s) de 2,5 litros</p>
					<p>{countHalfL} lata(s) de meio litro</p>
				</div>
			</div>
		</div> /*Início seção do texto de resultado*/
		)
}
export default ModalResult;