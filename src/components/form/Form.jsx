import React, {useState} from "react";
import ModalResult from "../modalResult/ModalResult"
import "./Form.css"
const Form = () =>{
	//useState responsável pelo estado do modal de resultados
	const [modalActive, setModalActive] = useState(false);
	//variáveis que recebem a quantidade de latas a serem utilizadas
	const [count18L, setCount18L] = useState(0);
	const [count3L, setCount3L] = useState(0);
	const [count2L, setCount2L] = useState(0);
	const [countHalfL, setCountHalfL] =  useState(0);
	//Variável que receberá a área total a ser pintada (excluindo portas e janelas)
	var totalPaintArea;
	//valores padrão para dimensões de altura e janela
	const doorH = 1.90, doorW = 0.80, windowH = 1.20, windowW = 2;
	//objeto com dados da primeira parede
	var firstWall ={
		id: 1,
		height: 0,
		width: 0,
		doorCount: 0,
		windowCount: 0,
		
	};

	//objeto com dados da segunda parede
	var secondWall ={
		id: 2,
		height: 0,
		width: 0,
		doorCount: 0,
		windowCount: 0,
	};

	//objeto com dados da terceira parede
	var thirdWall ={
		id: 3,
		height: 0,
		width: 0,
		doorCount: 0,
		windowCount: 0,
	};

	//objeto com dados da quarta parede
	var fourthWall ={
		id: 4,
		height: 0,
		width: 0,
		doorCount: 0,
		windowCount: 0,
	};
	


	//máscara para que apenas números reais positivos sejam inseridos nos campos que envolvem medidas
	function measuresMask(e){
		e.preventDefault();
		if(/[0-9,]/g.test(e.key)){
			//impedindo usuário de adicionar mais de uma vírgula
			if(/[,]/g.test(e.key) && e.target.value.indexOf(",") !== -1){
				e.target.value +="";
			}
			else{
				e.target.value+= e.key;
			}
		}
	}	
	//máscara para que apenas números inteiros positivos sejam inseridos nos campos que envolvem quantidade
	function countMask(e){
		e.preventDefault();
		if(/[0-9]/g.test(e.key)){
			e.target.value+=e.key;
		}
	}

	function handleSubmit(e){
		e.preventDefault();

		//verificando se há campos não preenchidos
		if(!verifyInputs()){
			alert('Há campos não preenchidos! Verifique e tente novamente!');
			return
		}
		//limpando campos de erros a cada submissão do formulário
		cleanErrorsDiv();
		//inserindo valores dos inputs nos objetos
		fillObjects();
		
		//verificando aplicação de requisitos das regras de negócio para a parede 1
		if(!verifyDoorSize(firstWall) || !verifyWallArea(firstWall)  || !verifyDoorsAndWindowsArea(firstWall) ||
			!verifyDoorSize(secondWall) || !verifyWallArea(secondWall)  || !verifyDoorsAndWindowsArea(secondWall) ||
			!verifyDoorSize(thirdWall) || !verifyWallArea(thirdWall)  || !verifyDoorsAndWindowsArea(thirdWall) ||
			!verifyDoorSize(fourthWall) || !verifyWallArea(fourthWall)  || !verifyDoorsAndWindowsArea(fourthWall)) {
			alert('Há problemas há serem resolvidos em alguma(s) parede(s). Verifique e tente novamente!');
			return;
		}

		//calculando área que precisará de tinta
		totalPaintArea = wallArea(firstWall) + wallArea(secondWall) + wallArea(thirdWall) + wallArea(fourthWall);
		
		//calculando quantidade de latas necessárias
		canCountCalc(totalPaintArea, count18L, count3L, count2L, countHalfL);

		//limpando inputs antes de abrir o modal de resultados
		clearInputs();

		//abrindo modal com resultados
		setModalActive(true);

	}

	//função para verificar campos não preenchidos
	function verifyInputs(){
		if(
			!document.getElementById("firstWallHeight").value || 
			!document.getElementById("firstWallWidth").value ||
			!document.getElementById("firstWallDoors").value ||
			!document.getElementById("firstWallWindows").value ||

			!document.getElementById("secondWallHeight").value || 
			!document.getElementById("secondWallWidth").value ||
			!document.getElementById("secondWallDoors").value ||
			!document.getElementById("secondWallWindows").value ||

			!document.getElementById("thirdWallHeight").value || 
			!document.getElementById("thirdWallWidth").value ||
			!document.getElementById("thirdWallDoors").value ||
			!document.getElementById("thirdWallWindows").value ||

			!document.getElementById("fourthWallHeight").value || 
			!document.getElementById("fourthWallWidth").value ||
			!document.getElementById("fourthWallDoors").value ||
			!document.getElementById("fourthWallWindows").value
			){
			return false;
		}
		return true
	}

	//função para limpar campos de erros do site
	function cleanErrorsDiv(){
		document.querySelector("#wallOneErrors").innerHTML = "";
		document.querySelector("#wallTwoErrors").innerHTML = "";
		document.querySelector("#wallThreeErrors").innerHTML = "";
		document.querySelector("#wallFourErrors").innerHTML = "";
	}


	function fillObjects(){
		//inserindo valores da parede 1 no objeto
		firstWall.height = parseFloat(document.getElementById("firstWallHeight").value.replace(",",".")).toFixed(2);
		firstWall.width = parseFloat(document.getElementById("firstWallWidth").value.replace(",",".")).toFixed(2);
		firstWall.doorCount = parseInt(document.getElementById("firstWallDoors").value);
		firstWall.windowCount = parseInt(document.getElementById("firstWallWindows").value);

		//inserindo valores da parede 2 no objeto
		secondWall.height = parseFloat(document.getElementById("secondWallHeight").value.replace(",",".")).toFixed(2);
		secondWall.width = parseFloat(document.getElementById("secondWallWidth").value.replace(",",".")).toFixed(2);
		secondWall.doorCount = parseInt(document.getElementById("secondWallDoors").value);
		secondWall.windowCount = parseInt(document.getElementById("secondWallWindows").value);

		//inserindo valores da parede 3 no objeto
		thirdWall.height = parseFloat(document.getElementById("thirdWallHeight").value.replace(",",".")).toFixed(2);
		thirdWall.width = parseFloat(document.getElementById("thirdWallWidth").value.replace(",",".")).toFixed(2);
		thirdWall.doorCount = parseInt(document.getElementById("thirdWallDoors").value);
		thirdWall.windowCount = parseInt(document.getElementById("thirdWallWindows").value);

		//inserindo valores da parede 4 no objeto
		fourthWall.height = parseFloat(document.getElementById("fourthWallHeight").value.replace(",",".")).toFixed(2);
		fourthWall.width = parseFloat(document.getElementById("fourthWallWidth").value.replace(",",".")).toFixed(2);
		fourthWall.doorCount = parseInt(document.getElementById("fourthWallDoors").value);
		fourthWall.windowCount = parseInt(document.getElementById("fourthWallWindows").value);
	}


	//função para verificar proporção das paredes em relação às portas 
	function verifyDoorSize(wall){
		if(wall.doorCount > 0 && wall.height < 2.20){
			let errorText = `<p>Atenção! A altura da parede deve ser de, no mínimo, 2,20m caso haja portas.</p>`;
			//verificando em qual campo ocorreu o erro e imprimindo a mensagem
			switch(wall.id){
				case 1:
				document.querySelector("#wallOneErrors").innerHTML += errorText;
				break;

				case 2:
				document.querySelector("#wallTwoErrors").innerHTML += errorText;
				break;

				case 3:
				document.querySelector("#wallThreeErrors").innerHTML += errorText;
				break;

				case 4:
				document.querySelector("#wallFourErrors").innerHTML += errorText;
				break;

				default:
				console.log("Nada aconteceu aqui!");
			}
			return false;
		}
		return true;
	}

	//função para verificar se área da parede corresponde ao especificado nas regras de negócio
	function verifyWallArea(wall){
		let errorText = `<p>Atenção! A área da parede deve ter entre 1m² e 50m².</p>
		<p>Recomendamos que diminua o tamanho da largura e/ou da altura da parede.</p>`
		if(wallArea(wall) > 50 || wallArea(wall) < 1){
			switch(wall.id){
				case 1:
				document.querySelector("#wallOneErrors").innerHTML += errorText;
				break;

				case 2:
				document.querySelector("#wallTwoErrors").innerHTML += errorText;
				break;

				case 3:
				document.querySelector("#wallThreeErrors").innerHTML += errorText;
				break;

				case 4:
				document.querySelector("#wallFourErrors").innerHTML += errorText;
				break;

				default:
				console.log("Nada aconteceu aqui!");
			}
			return false;
		}
		return true;
	}

	function verifyDoorsAndWindowsArea(wall){
		let errorText = `<p>Atenção! As portas e janelas não podem cobrir mais que 50% da área da parede!</p>
		<p>Recomendamos que você diminua a quantidade de portas e/ou janelas, ou aumente o tamanho da parede.</p>`;
		if((wall.doorCount*(doorW*doorH)) + (wall.windowCount*(windowW * windowH)) > ((wall.height*wallArea(wall))/2)){
			switch(wall.id){
				case 1:
				document.querySelector("#wallOneErrors").innerHTML += errorText;
				break;

				case 2:
				document.querySelector("#wallTwoErrors").innerHTML += errorText;
				break;

				case 3:
				document.querySelector("#wallThreeErrors").innerHTML += errorText;
				break;

				case 4:
				document.querySelector("#wallFourErrors").innerHTML += errorText;
				break;

				default:
				console.log("Nada aconteceu aqui!");
			}
			return false;
		}
		return true;
	}

	//função que calcula área das paredes
	function wallArea(wall){
		return ((wall.height * wall.width));
	}

	//função para limpar input's
	function clearInputs(){
		let clear = document.getElementById("cleanButton")
		clear.click();
	}

	function canCountCalc(totalPaintArea){
		let count18l = 0, count3l = 0, count2l = 0, countHalf = 0;
		//verificando quantidades de lata de 18l até que não seja mais economicamente viável
		while(totalPaintArea -  (18*5) > 0){
			totalPaintArea = totalPaintArea - (18*5);
			count18l++;
		}
		setCount18L(count18l);

		//verificando quantidades de lata de 3,6l até que não seja mais economicamente viável
		while(totalPaintArea -  (3.6*5) > 0){
			totalPaintArea = totalPaintArea - (3.6*5);
			count3l++;
		}
		setCount3L(count3l);

		//verificando quantidades de lata de 2,5l até que não seja mais economicamente viável
		while(totalPaintArea -  (2.5*5) > 0){
			totalPaintArea = totalPaintArea - (2.5*5);
			count2l++;
		}
		setCount2L(count2l);
		
		//O que sobrar será pintado utilizando latas de meio-litro
		while(totalPaintArea > 0){
			totalPaintArea = totalPaintArea - (0.5*5);
			countHalf++;
		}
		setCountHalfL(countHalf);
		
	} 

	return(
		<>
			<div className="containerForm"> {/*início do container*/}
			 	<h2>Formulário</h2>
			 	<form> {/*Início do formulário*/}
			 		<div className="wallOrderOne"> {/*início da sessão da primeira parede*/}
				 		<div className="infoTarget">
					 		<label htmlFor="firstWallHeight">Insira a altura da parede 1</label>
					 		<input type="text" placeholder="ex: 1,84 (metros)" 
					 		name="firstWallHeight" id="firstWallHeight"
					 		onKeyPress={(e)=>measuresMask(e)} onPaste={(e)=>{e.preventDefault()}} />
				 		</div>
				 		<div className="infoTarget">
					 		<label htmlFor="firstWallWidth">Insira a largura da parede 1</label>
					 		<input type="text" placeholder="ex: 1,84 (metros)" 
					 		name="firstWallWidth" id="firstWallWidth"
					 		onKeyPress={(e)=>measuresMask(e)} onPaste={(e)=>{e.preventDefault()}} />
				 		</div>
				 		<div className="infoTarget">
					 		<label htmlFor="firstWallWindows">Insira quantas janelas terá a parede 1</label>
					 		<input type="text" name="firstWallWindows" id="firstWallWindows"
					 		onKeyPress={(e)=>countMask(e)} onPaste={(e)=>{e.preventDefault()}}/>
				 		</div>
				 		<div className="infoTarget">
					 		<label htmlFor="firstWallDoors">Insira quantas portas terá a parede 1</label>
					 		<input type="text"  name="firstWallDoors" id="firstWallDoors"
					 		onKeyPress={(e)=>countMask(e)} onPaste={(e)=>{e.preventDefault()}}/>
				 		</div>
				 		{/*Div onde serrão inseridos possíveis erros/violações às regras de negócio*/}
				 		<div id="wallOneErrors"></div>
				 	</div> {/*Fim da sessão da primeira parede*/}

				 	<div className="wallOrderTwo"> {/*início da seção da segunda parede*/}
				 		<div className="infoTarget">
					 		<label htmlFor="secondWallHeight">Insira a altura da parede 2</label>
					 		<input type="text" placeholder="ex: 1,84 (metros)" 
					 		name="secondWallHeight" id="secondWallHeight"
					 		onKeyPress={(e)=>measuresMask(e)} onPaste={(e)=>{e.preventDefault()}} />
				 		</div>
				 		<div className="infoTarget">
					 		<label htmlFor="secondWallWidth">Insira a largura da parede 2</label>
					 		<input type="text" placeholder="ex: 1,84 (metros)" 
					 		name="secondWallWidth" id="secondWallWidth"
					 		 onKeyPress={(e)=>measuresMask(e)} onPaste={(e)=>{e.preventDefault()}}/>
				 		</div>
				 		<div className="infoTarget">
					 		<label htmlFor="secondWalllWindows">Insira quantas janelas terá a parede 2</label>
					 		<input type="text"  
					 		name="secondWallWindows" id="secondWallWindows"
					 		onKeyPress={(e)=>countMask(e)} onPaste={(e)=>{e.preventDefault()}}/>
				 		</div>
				 		<div className="infoTarget">
					 		<label htmlFor="secondWallDoors">Insira quantas portas terá a parede 2</label>
					 		<input type="text"  
					 		name="secondWallWindows" id="secondWallDoors"
					 		onKeyPress={(e)=>countMask(e)} onPaste={(e)=>{e.preventDefault()}}/>
				 		</div>
				 		{/*Div onde serrão inseridos possíveis erros/violações às regras de negócio*/}
				 		<div id="wallTwoErrors"></div>
			 		</div> {/*fim da seção da segunda parede*/}

			 		<div className="wallOrderThree">{/*início da seção da terceira parede*/}
				 		<div className="infoTarget">
					 		<label htmlFor="thridWallHeight">Insira a altura da parede 3</label>
					 		<input type="text" placeholder="ex: 1,84 (metros)" 
					 		name="thirdWallHeight" id="thirdWallHeight"
					 		 onKeyPress={(e)=>measuresMask(e)} onPaste={(e)=>{e.preventDefault()}}/>
				 		</div>
				 		<div className="infoTarget">
					 		<label htmlFor="thirdWallWidth">Insira a largura da parede 3</label>
					 		<input type="text" placeholder="ex: 1,84 (metros)" 
					 		name="thirdWallWidth" id="thirdWallWidth"
					 		 onKeyPress={(e)=>measuresMask(e)} onPaste={(e)=>{e.preventDefault()}}/>
				 		</div>
				 		<div className="infoTarget">
					 		<label htmlFor="thirdWalllWindows">Insira quantas janelas terá a parede 3</label>
					 		<input type="text"  
					 		name="thirdWallWindows" id="thirdWallWindows"
					 		onKeyPress={(e)=>countMask(e)} onPaste={(e)=>{e.preventDefault()}}/>
				 		</div>
				 		<div className="infoTarget">
					 		<label htmlFor="thirdWallDoors">Insira quantas portas terá a parede 3</label>
					 		<input type="text"  
					 		name="thirdWallWindows" id="thirdWallDoors"
					 		onKeyPress={(e)=>countMask(e)} onPaste={(e)=>{e.preventDefault()}}/>
				 		</div>
				 		{/*Div onde serrão inseridos possíveis erros/violações às regras de negócio*/}
				 		<div id="wallThreeErrors"></div>
				 	</div> {/*Fim da seção da terceira parede*/}

				 	<div className="wallOrderFour"> {/*início da seção da quarta parede*/}
				 		<div className="infoTarget">
					 		<label htmlFor="foruthWallHeight">Insira a altura da parede 4</label>
					 		<input required type="text" placeholder="ex: 1,84 (metros)" 
					 		name="fourthWallHeight" id="fourthWallHeight"
					 		 onKeyPress={(e)=>measuresMask(e)} onPaste={(e)=>{e.preventDefault()}}/>
				 		</div>
				 		<div className="infoTarget">
					 		<label htmlFor="fourthWallWidth">Insira a largura da parede 4</label>
					 		<input type="text" placeholder="ex: 1,84 (metros)" 
					 		name="fourthWallWidth noPaste" id="fourthWallWidth"
					 		 onKeyPress={(e)=>measuresMask(e)} onPaste={(e)=>{e.preventDefault()}}/>
				 		</div>
				 		<div className="infoTarget">
					 		<label htmlFor="fourthWalllWindows">Insira quantas janelas terá a parede 4</label>
					 		<input type="text"  
					 		name="fourthWallWindows" id="fourthWallWindows"
					 		onKeyPress={(e)=>countMask(e)} onPaste={(e)=>{e.preventDefault()}}/>
				 		</div>
				 		<div className="infoTarget">
					 		<label htmlFor="fourthWallDoors">Insira quantas portas terá a parede 4</label>
					 		<input type="text"  
					 		name="fourthWallWindows" id="fourthWallDoors"
					 		onKeyPress={(e)=>countMask(e)} onPaste={(e)=>{e.preventDefault()}}/>
				 		</div>
				 		{/*Div onde serrão inseridos possíveis erros/violações às regras de negócio*/}
				 		<div id="wallFourErrors"></div>
			 		</div> {/*Fim da seção da quarta parede*/}
			 		<div className="divButtons">
				 		<button onClick={(e)=>handleSubmit(e)}>Calcular</button>
				 		<button type="reset" id="cleanButton">Limpar campos</button>
				 	</div>
			 	</form> {/*Fim do formulário*/}
			</div> {/*Fim do container*/}
			{modalActive && <ModalResult setModalActive={setModalActive} count18L = {count18L} count3L = {count3L} count2L = {count2L} countHalfL = {countHalfL}/>}
		</>
		)
}
export default Form;