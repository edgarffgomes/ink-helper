import React from "react";
import "./Footer.css";
const Footer = ()=>{
	return(
		<footer>
			<div className="footerText">
				<p>
					Código desenvolvido por Edgar Gomes
				</p>
				<p>
					Desenvolvedor Front-End Jr. na Digital Republic
				</p>
			</div>
		</footer>
		)
}
export default Footer;