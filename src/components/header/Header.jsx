import React from "react";
import "./Header.css"
//CanIcon foi baixado do  website flaticon, e é de autoria do usuário Adib Sulthon
import CanIcon from "../../icons/ink-can.png"
const Header = ()=>{
	return(
		<header className="containerHeader">
			<h3>Ink Helper </h3>
			<img alt="ilustração de lata de tinta." src={CanIcon}/>
		</header>
		)
}
export default Header;