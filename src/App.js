import React from "react";
import "./App.css";
import Header from "./components/header/Header";
import Form from "./components/form/Form";
import Intro from "./components/intro/Introduction";
import Footer from "./components/footer/Footer";
function App() {
  return (
    <>
      <Header />
      <Intro />
      <Form />
      <Footer />
    </>
  );
}
export default App;
